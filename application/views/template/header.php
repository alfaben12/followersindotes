<script src="<?= base_url() . $this->load->config->item('library'); ?>jquery/jquery-3.2.1.min.js"></script>
<script src="<?= base_url() . $this->load->config->item('library'); ?>jquery/jquery-ui.js"></script>
<script src="<?= base_url() . $this->load->config->item('library'); ?>jquery/jquery.inputmask.bundle.js"></script>
<script src="<?= base_url() . $this->load->config->item('library'); ?>jquery/jquery.blockUI.js"></script>
<script src="<?= base_url() . $this->load->config->item('library'); ?>blocknonnumber/blocknonnumber.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".Uppercase").change(function(){
			$(this).val($(this).val().toUpperCase());
		});
		/* DATEPICKER ADD ATTRIBUTE READLONLY */
		$(".Datepicker").prop("readonly", true);
		$(".Datetimepicker").prop("readonly", true);
		$(".Datepicker_start").prop("readonly", true);
		$(".Datepicker_end").prop("readonly", true);
		$(".Datetimepicker_start").prop("readonly", true);
		$(".Datetimepicker_end").prop("readonly", true);
	});
</script>
<script type="text/javascript">
	jQuery(function(){
		/* DATEPICKER */
		jQuery(".Datepicker").datetimepicker({
			format:"Y/m/d",
			timepicker:false,
			closeOnDateSelect:true
		});
		
		/* DATETIMEPICKER */
		jQuery(".Datetimepicker").datetimepicker({
			format:"Y/m/d H:00:00",
			timepicker:true
		});

		/* RANGE DATEPICKER */
		jQuery(".Datepicker_start").datetimepicker({
			format:"Y/m/d",
			onShow:function( ct ){
				this.setOptions({
					maxDate:jQuery(".Datepicker_end").val() ? jQuery(".Datepicker_end").val() : false
				})
			},
			timepicker:false,
			closeOnDateSelect:true
		});

		jQuery(".Datepicker_end").datetimepicker({
			format:"Y/m/d",
			onShow:function( ct ){
				this.setOptions({
					minDate:jQuery(".Datepicker_start").val() ? jQuery(".Datepicker_start").val() : false
				})
			},
			timepicker:false,
			closeOnDateSelect:true
		});

		/* RANGE DATE & TIME PICKER */
		jQuery(".Datetimepicker_start").datetimepicker({
			format:"Y/m/d H:00:00",
			onShow:function( ct ){
				this.setOptions({
					maxDate:jQuery(".Datetimepicker_end").val() ? jQuery(".Datetimepicker_end").val() : false
				})
			},
			timepicker:true,
			closeOnDateSelect:true
		});

		jQuery(".Datetimepicker_end").datetimepicker({
			format:"Y/m/d H:00:00",
			onShow:function( ct ){
				this.setOptions({
					minDate:jQuery(".Datetimepicker_start").val() ? jQuery(".Datetimepicker_start").val() : false
				})
			},
			timepicker:true,
			closeOnDateSelect:true
		});
	});
</script>
<script type="text/javascript">
	var javascript_url_module = "<?= config_item('url_javascript'); ?>";
	var ajax_url_module = "<?= config_item('url_ajax'); ?>";
	var url_module = "<?= config_item('url_module'); ?>";
	var base_url = "<?= base_url(); ?>";
	var uri_laporan = "<?= str_replace('laporan', 'ambil', $this->uri->segment(2)); ?>";
</script>
<style type="text/css">
	.error{
		color: red;
		font-size: 13px;
	}
</style>
<?php if(isset($includes_for_layout['css']) AND count($includes_for_layout['css']) > 0): ?>
	<?php foreach($includes_for_layout['css'] as $css): ?>
		<?php if($css['script'] == FALSE): ?>
			<link rel="stylesheet" type="text/css" href="<?php echo $css['file']; ?>"<?php echo ($css['options'] === NULL ? '' : ' media="' . $css['options'] . '"'); ?>>
		<?php endif; ?>
		<?php if($css['script'] == TRUE): ?>
			<style type="text/css"><?php echo $css['file']; ?>

		</style>
	<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
<?php if(isset($includes_for_layout['js']) AND count($includes_for_layout['js']) > 0): ?>
	<?php foreach($includes_for_layout['js'] as $js): ?>
		<?php if($js['script'] == FALSE AND $js['options'] == 'header'): ?>
			<script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
		<?php endif; ?>
		<?php if($js['script'] == TRUE AND $js['options'] == 'header'): ?>
			<script type="text/javascript"><?php echo $js['file']; ?>

		</script>
	<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="<?= base_url('welcomes'); ?>"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<!-- <form class="navbar-form navbar-left">
					<div class="input-group">
						<input type="text" value="" class="form-control" placeholder="Search dashboard...">
						<span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
					</div>
				</form> -->
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?= base_url() . config_item('stylesheet') ?>img/user3.png" class="img-circle" alt="Avatar"> <span><?=$this->session->userdata("nama")?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="<?= base_url('logins/keluar'); ?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="<?= base_url('users'); ?>" class=""><i class="lnr lnr-home"></i> <span>User</span></a></li>
						<li><a href="<?= base_url('produks'); ?>" class=""><i class="lnr lnr-file-empty"></i><span>Produk</span></a></li>
						<li><a href="<?= base_url('deposits'); ?>" class=""><i class="lnr lnr-dice"></i> <span>Deposit</span></a></li>
						<li><a href="<?= base_url('transaksis'); ?>" class=""><i class="lnr lnr-dice"></i> <span>Transaksi</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->

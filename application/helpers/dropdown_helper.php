<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('user_saldo')) {
	function user_saldo($userid) {
		$CI = &get_instance();
		$CI->load->database();

		$data = array();
		$CI->db->select('
			user.saldo
			');
		$CI->db->from('user');
		$CI->db->where('id', $userid);
		$result = $CI->db->get();
		$data = $result->row()->saldo;
		return($data);
	}
}

if(!function_exists('produk_stok')) {
	function produk_stok($produkid) {
		$CI = &get_instance();
		$CI->load->database();

		$data = array();
		$CI->db->select('
			produk.stok
			');
		$CI->db->from('produk');
		$CI->db->where('id', $produkid);
		$result = $CI->db->get();
		$data = $result->row()->stok;
		return($data);
	}
}

/*-------- USER DROPDOWN --------*/
if(!function_exists('user_dropdown')) {
	function user_dropdown() {
		$CI = &get_instance();
		$CI->load->database();

		$data = array();
		$CI->db->select('
			user.id,
			user.nama
			');
		$CI->db->from('user');
		$CI->db->order_by('nama','asc');
		$data[''] = '';
		$result = $CI->db->get();
		if($result->num_rows() > 0) {
			foreach ($result->result_array() as $key => $val) {
				$data[$val['id']] = $val['nama'];
			}
		}
		return($data);
	}
}

/*-------- USER DROPDOWN --------*/
if(!function_exists('produk_dropdown')) {
	function produk_dropdown() {
		$CI = &get_instance();
		$CI->load->database();

		$data = array();
		$CI->db->select('
			produk.id,
			produk.nama_produk
			');
		$CI->db->from('produk');
		$CI->db->order_by('nama_produk','asc');
		$data[''] = '';
		$result = $CI->db->get();
		if($result->num_rows() > 0) {
			foreach ($result->result_array() as $key => $val) {
				$data[$val['id']] = $val['nama_produk'];
			}
		}
		return($data);
	}
}
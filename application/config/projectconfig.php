<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['ref_penjualan']			= 'PJL0';
$config['ref_pembelian']			= 'PBL0';
$config['ref_pembelian_bahan']		= 'PBH0';
$config['ref_produksi']				= 'PRS0';
$config['ref_pembayaran']			= 'PBY0';
$config['library'] 					= 'library/';
$config['stylesheet'] 				= 'stylesheet/';

/* SESSION NOTIFICATION */
$config['login_success'] = '
<script>
	$(document).ready(function(){
		$(document).ready(function(){
			Command: toastr["success"]("", "Login berhasil !")

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "10000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		});
	});
</script>
';

$config['login_failed'] = '
<script>
	$(document).ready(function(){
		$(document).ready(function(){
			Command: toastr["error"]("", "Username atau password salah !")

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "10000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		});
	});
</script>
';

$config['denied'] = '
<script>
	$(document).ready(function(){
		$(document).ready(function(){
			Command: toastr["error"]("", "Hak akses ditolak !")

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "10000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		});
	});
</script>
';

$config['saved'] = '
<script>
	$(document).ready(function(){
		$(document).ready(function(){
			Command: toastr["success"]("", "Data Berhasil Disimpan")

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		});
	});
</script>
';

$config['updated'] = '
<script>
	$(document).ready(function(){
		$(document).ready(function(){
			Command: toastr["info"]("", "Data berhasil di Update")

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		});
	});
</script>
';

$config['deleted'] = '
<script>
	$(document).ready(function(){
		$(document).ready(function(){
			Command: toastr["error"]("", "Data Berhasil Dihapus")

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		});
	});
</script>
';
$config['limited'] = '
<script>
	$(document).ready(function(){
		$(document).ready(function(){
			Command: toastr["error"]("", "Stok Tidak Mencukupi")

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		});
	});
</script>
';
?>
<?= $this->session->flashdata('pesan'); ?>
<h2 class="page-title">Deposit</h2>
<a href="<?= base_url($this->uri->segment(1) .'/tambah'); ?>" class="btn btn-primary">Tambah <b>+</b></a>
<div class="row" style="margin-top: 20px;">
	<div class="col-md-12">
		<!-- TABLE STRIPED -->
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Daftar Produk</h3>
			</div>
			<div class="panel-body">
				<table class="datatables table table-striped">
					<thead>
						<tr>
							<th width="5">No</th>
							<th>User Tujuan</th>
							<th>Nama Pengirim</th>
							<th>Jumlah</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($data as $key => $val) {
							?>
							<tr>
								<td><?= $key + 1 ?></td>
								<td><?= $val['user_tujuan'] ?></td>
								<td><?= $val['nama_pengirim'] ?></td>
								<td><?= $val['jumlah'] ?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END TABLE STRIPED -->
	</div>
</div>
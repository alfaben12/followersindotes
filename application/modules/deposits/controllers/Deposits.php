<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Deposits extends CI_Controller{
	
	function __construct(){
		parent :: __construct();
		/* Load model */
		$this->load->model('deposit');
		$this->load->model('users/user');
	}

	function index(){
		$result = $this->deposit->get_all_data();
		$data['data'] = $result;
		$this->template->write_view('list', $data);
	}

	function tambah(){
		$data['user'] = user_dropdown();
		$this->template->write_view('add', $data);
	}

	function prosess_tambah(){
		/* atur form validasi */
		$this->form_validation->set_rules('txt_userid', 'User Tujuan', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_nama', 'Nama Pengirim', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_jumlah', 'Harga', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<font color="red">', '</font>');

		if($this->form_validation->run() == FALSE) {
			$data['user'] = user_dropdown();
			$this->template->write_view('add', $data);
		} else {
			/* membuat variable post data */
			$userid = $this->input->post('txt_userid');
			$nama = $this->input->post('txt_nama');
			$jumlah = $this->input->post('txt_jumlah');

			$this->db->trans_start();
			/* membuat value */
			$this->deposit->setUserID($userid);
			$this->deposit->setNamaPengirim($nama);
			$this->deposit->setJumlah($jumlah);

			$this->user->setID($userid);
			$this->user->setSaldo($jumlah + user_saldo($userid));
			/* fungsi insert data ke database */
			$this->deposit->insert_data();
			$this->user->update_saldo();
			$this->db->trans_complete();

			/* flashdata info error/sukses */
			$this->session->set_flashdata('pesan', config_item('saved'));
			redirect($this->uri->segment(1). '/index');
		}
	}
}
<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Deposit extends CI_Model{

	private $id;
	private $userid;
	private $nama_pengirim;
	private $jumlah;

	function __construct(){parent :: __construct();}
	function getID(){return $this->id;}
	function setID($id){$this->id = $id;}
	function getUserID(){return $this->userid;}
	function setUserID($userid){$this->userid = $userid;}
	function getNamaPengirim(){return $this->nama_pengirim;}
	function setNamaPengirim($nama_pengirim){$this->nama_pengirim = $nama_pengirim;}
	function getJumlah(){return $this->jumlah;}
	function setJumlah($jumlah){$this->jumlah = $jumlah;}

function insert_data(){
		$data = array(
			'userid' => $this->getUserID(),
			'nama_pengirim' => $this->getNamaPengirim(),
			'jumlah' => $this->getJumlah(),
		);
		$this->db->insert('deposit', $data);
	}

	function get_all_data(){
		$this->db->select('
			deposit.nama_pengirim,
			deposit.jumlah,
			user.nama AS user_tujuan
			');
		$this->db->from('deposit');
		$this->db->join('user', 'deposit.userid = user.id');
		$query = $this->db->get();
		return $query->result_array();
	}
}
<?php
foreach ($data as $key => $val) {
	?>
	<h3 class="page-title">Edit User</h3>
	<?= form_open($this->uri->segment(1).'/prosess_edit/id/'. $arr); ?>
	<div class="row">
		<div class="col-md-6">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Data User</h3>
				</div>
				<div class="panel-body">
					<input class="form-control" placeholder="Nama" type="text" name="txt_nama" value="<?= set_value('txt_nama', $val['nama']) ?>">
					<?= form_error('txt_nama'); ?>
					<br>
					<input class="form-control" placeholder="Username" type="text" name="txt_username" value="<?= set_value('txt_username', $val['username']) ?>">
					<?= form_error('txt_username'); ?>
					<br>
					<input class="form-control" placeholder="Password" type="password" name="txt_password" value="<?= set_value('txt_password') ?>">
					<?= form_error('txt_password'); ?>
					<br>
					<input class="form-control" placeholder="Saldo" type="text" name="txt_saldo" value="<?= set_value('txt_saldo', $val['saldo']) ?>" onkeypress="return blockNonNumbers(this, event, true, false);">
					<?= form_error('txt_saldo'); ?>
					<br>
					<div style="text-align: right">
						<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Submit </button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?= form_close(); ?>
	<?php
}
?>
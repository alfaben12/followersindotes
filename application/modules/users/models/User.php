<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Model{

	private $id;
	private $nama;
	private $username;
	private $password;
	private $saldo;

	function __construct(){parent :: __construct();}
	function getID(){return $this->id;}
	function setID($id){$this->id = $id;}
	function getNama(){return $this->nama;}
	function setNama($nama){$this->nama = $nama;}
	function getUsername(){return $this->username;}
	function setUsername($username){$this->username = $username;}
	function getPassword(){return $this->password;}
	function setPassword($password){$this->password = $password;}
	function getSaldo(){return $this->saldo;}
	function setSaldo($saldo){$this->saldo = $saldo;}

	function insert_data(){
		$data = array(
			'nama' => $this->getNama(),
			'username' => $this->getUsername(),
			'password' => $this->getPassword(),
			'saldo' => $this->getSaldo()
		);
		$this->db->insert('user', $data);
	}

	function update_data(){
		$data = array(
			'nama' => $this->getNama(),
			'username' => $this->getUsername(),
			'password' => $this->getPassword(),
			'saldo' => $this->getSaldo()
		);
		$this->db->where('id', $this->getID());
		$this->db->update('user', $data);
	}

	function update_saldo(){
		$data = array(
			'saldo' => $this->getSaldo()
		);
		$this->db->where('id', $this->getID());
		$this->db->update('user', $data);
	}

	function get_all_data(){
		$this->db->select('*');
		$this->db->from('user');
		$query = $this->db->get();
		return $query->result_array();
	}

	function ambil_data(){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id', $this->getID());
		$query = $this->db->get();
		return $query->result_array();
	}

	function delete_data(){
		$this->db->where('id', $this->getID());
		$this->db->delete('user');
	}

}
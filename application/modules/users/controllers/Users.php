<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Controller{
	
	function __construct(){
		parent :: __construct();
		/* Load model */
		$this->load->model('user');
	}

	function index(){
		$result = $this->user->get_all_data();
		$data['data'] = $result;
		$this->template->write_view('list', $data);
	}

	function tambah(){
		$this->template->write_view('add');
	}

	function prosess_tambah(){
		/* atur form validasi */
		$this->form_validation->set_rules('txt_nama', 'Nama user', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_saldo', 'Saldo', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<font color="red">', '</font>');

		if($this->form_validation->run() == FALSE) {
			/* data dropdown */
			$this->template->write_view('add');
		} else {
			/* membuat variable post data */
			$nama = $this->input->post('txt_nama');
			$username = $this->input->post('txt_username');
			$password = $this->input->post('txt_password');
			$saldo = $this->input->post('txt_saldo');

			$this->db->trans_start();
			/* membuat value */
			$this->user->setNama($nama);
			$this->user->setUsername($username);
			$this->user->setPassword($password);
			$this->user->setSaldo($saldo);

			/* fungsi insert data ke database */
			$this->user->insert_data();
			$this->db->trans_complete();

			/* flashdata info error/sukses */
			$this->session->set_flashdata('pesan', config_item('saved'));
			redirect($this->uri->segment(1). '/index');
		}
	}

	function edit(){
		$id = $this->uri->segment(4);
		$data['arr'] = $id;
		
		$this->user->setID($data['arr']);

		$result = $this->user->ambil_data();
		$data['data'] = $result;
		$this->template->write_view('edit', $data);
	}

	function prosess_edit(){
		$id = $this->uri->segment(4);
		$data['arr'] = $id;
		
		$this->user->setID($data['arr']);
		/* atur form validasi */
		$this->form_validation->set_rules('txt_nama', 'Nama user', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_saldo', 'Saldo', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<font color="red">', '</font>');

		if($this->form_validation->run() == FALSE) {
			$result = $this->user->ambil_data();
			$data['data'] = $result;
			$this->template->write_view('edit');
		} else {
			/* membuat variable post data */
			$nama = $this->input->post('txt_nama');
			$username = $this->input->post('txt_username');
			$password = $this->input->post('txt_password');
			$saldo = $this->input->post('txt_saldo');

			$this->db->trans_start();
			/* membuat value */
			$this->user->setNama($nama);
			$this->user->setUsername($username);
			$this->user->setPassword($password);
			$this->user->setSaldo($saldo);

			/* fungsi insert data ke database */
			$this->user->update_data();
			$this->db->trans_complete();

			/* flashdata info error/sukses */
			$this->session->set_flashdata('pesan', config_item('saved'));
			redirect($this->uri->segment(1). '/index');
		}
	}

	function hapus(){
		/* atur parameter data */
		$arr = $this->uri->segment(4);
		$data['arr'] = $arr;

		/* membuat value */
		$this->user->setID($data['arr']);

		/* fungsi hapus data dari database */
		$this->user->delete_data();

		/* flashdata info error/sukses */
		$this->session->set_flashdata('pesan', config_item('deleted'));
		redirect($this->uri->segment(1). '/index');
	}
}
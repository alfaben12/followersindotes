<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Transaksis extends CI_Controller{
	
	function __construct(){
		parent :: __construct();
		/* Load model */
		$this->load->model('transaksi');
		$this->load->model('produks/produk');
	}

	function index(){
		$result = $this->transaksi->get_all_data();
		$data['data'] = $result;
		$this->template->write_view('list', $data);
	}

	function tambah(){
		$data['user'] = user_dropdown();
		$data['produk'] = produk_dropdown();
		$this->template->write_view('add', $data);
	}

	function prosess_tambah(){
		/* atur form validasi */
		$this->form_validation->set_rules('txt_userid', 'User', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_produkid', 'Produk', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_jumlah', 'Harga', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<font color="red">', '</font>');

		if($this->form_validation->run() == FALSE) {
			$data['user'] = user_dropdown();
			$data['produk'] = produk_dropdown();
			$this->template->write_view('add', $data);
		} else {
			/* membuat variable post data */
			$userid = $this->input->post('txt_userid');
			$produkid = $this->input->post('txt_produkid');
			$jumlah = $this->input->post('txt_jumlah');

			$this->db->trans_start();
			/* membuat value */
			$this->transaksi->setUserID($userid);
			$this->transaksi->setProdukID($produkid);
			$this->transaksi->setJumlah($jumlah);

			$this->produk->setID($produkid);
			if (produk_stok($produkid) >= $jumlah) {
				$this->produk->setStok(produk_stok($produkid) - $jumlah);

				/* fungsi insert data ke database */
				$this->transaksi->insert_data();
				$this->produk->update_stok();
				$this->db->trans_complete();
				/* flashdata info error/sukses */
				$this->session->set_flashdata('pesan', config_item('saved'));
				redirect($this->uri->segment(1). '/index');
			}else{
				$this->session->set_flashdata('pesan', config_item('limited'));
				redirect($this->uri->segment(1). '/index');
			}
		}
	}
}
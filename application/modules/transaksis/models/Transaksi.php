<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Transaksi extends CI_Model{

	private $id;
	private $userid;
	private $produkid;
	private $jumlah;

	function __construct(){parent :: __construct();}
	function getID(){return $this->id;}
	function setID($id){$this->id = $id;}
	function getUserID(){return $this->userid;}
	function setUserID($userid){$this->userid = $userid;}
	function getProdukID(){return $this->produkid;}
	function setProdukID($produkid){$this->produkid = $produkid;}
	function getJumlah(){return $this->jumlah;}
	function setJumlah($jumlah){$this->jumlah = $jumlah;}

	function insert_data(){
		$data = array(
			'userid' => $this->getUserID(),
			'produkid' => $this->getProdukID(),
			'jumlah' => $this->getJumlah(),
		);
		$this->db->insert('transaksi', $data);
	}

	function get_all_data(){
		$this->db->select('
			transaksi.produkid,
			transaksi.jumlah,
			user.nama AS user,
			produk.nama_produk
			');
		$this->db->from('transaksi');
		$this->db->join('user', 'transaksi.userid = user.id');
		$this->db->join('produk', 'transaksi.produkid = produk.id');
		$query = $this->db->get();
		return $query->result_array();
	}
}
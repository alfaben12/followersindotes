<h3 class="page-title">Tambah Transaksi Baru</h3>
<?= form_open($this->uri->segment(1).'/prosess_tambah'); ?>
<div class="row">
	<div class="col-md-6">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Data Transaksi</h3>
			</div>
			<div class="panel-body">
				<?= form_dropdown('txt_userid', $user, set_value('txt_userid'), 'data-placeholder="User" class="form-control select2-single"') ?>
				<br>
				<?= form_error('txt_userid'); ?>
				<br>
				<?= form_dropdown('txt_produkid', $produk, set_value('txt_produkid'), 'data-placeholder="Produk" class="form-control select2-single"') ?>
				<br/>
				<?= form_error('txt_produkid'); ?>
				<br>
				<input class="form-control" placeholder="Jumlah" type="text" name="txt_jumlah" value="<?= set_value('txt_jumlah') ?>" onkeypress="return blockNonNumbers(this, event, true, false);">
				<?= form_error('txt_jumlah'); ?>
				<br>
				<div style="text-align: right">
					<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Submit </button>
				</div>
			</div>
		</div>
	</div>
</div>
<?= form_close(); ?>
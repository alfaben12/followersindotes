<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Produks extends CI_Controller{
	
	function __construct(){
		parent :: __construct();
		/* Load model */
		$this->load->model('produk');
	}

	function index(){
		$result = $this->produk->get_all_data();
		$data['data'] = $result;
		$this->template->write_view('list', $data);
	}

	function tambah(){
		$this->template->write_view('add');
	}

	function prosess_tambah(){
		/* atur form validasi */
		$this->form_validation->set_rules('txt_nama', 'Nama Produk', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_harga', 'Harga', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_stok', 'Stok', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<font color="red">', '</font>');

		if($this->form_validation->run() == FALSE) {
			/* data dropdown */
			$this->template->write_view('add');
		} else {
			/* membuat variable post data */
			$nama = $this->input->post('txt_nama');
			$harga = $this->input->post('txt_harga');
			$stok = $this->input->post('txt_stok');

			$this->db->trans_start();
			/* membuat value */
			$this->produk->setNamaProduk($nama);
			$this->produk->setHarga($harga);
			$this->produk->setStok($stok);

			/* fungsi insert data ke database */
			$this->produk->insert_data();
			$this->db->trans_complete();

			/* flashdata info error/sukses */
			$this->session->set_flashdata('pesan', config_item('saved'));
			redirect($this->uri->segment(1). '/index');
		}
	}

	function edit(){
		$id = $this->uri->segment(4);
		$data['arr'] = $id;
		
		$this->produk->setID($data['arr']);

		$result = $this->produk->ambil_data();
		$data['data'] = $result;
		$this->template->write_view('edit', $data);
	}

	function prosess_edit(){
		$id = $this->uri->segment(4);
		$data['arr'] = $id;
		
		$this->produk->setID($data['arr']);
		/* atur form validasi */
		$this->form_validation->set_rules('txt_nama', 'Nama produk', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_harga', 'Harga', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_stok', 'Stok', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<font color="red">', '</font>');

		if($this->form_validation->run() == FALSE) {
			$result = $this->produk->ambil_data();
			$data['data'] = $result;
			$this->template->write_view('edit');
		} else {
			/* membuat variable post data */
			$nama = $this->input->post('txt_nama');
			$harga = $this->input->post('txt_harga');
			$stok = $this->input->post('txt_stok');

			$this->db->trans_start();
			/* membuat value */
			$this->produk->setNamaProduk($nama);
			$this->produk->setHarga($harga);
			$this->produk->setStok($stok);

			/* fungsi insert data ke database */
			$this->produk->update_data();
			$this->db->trans_complete();

			/* flashdata info error/sukses */
			$this->session->set_flashdata('pesan', config_item('saved'));
			redirect($this->uri->segment(1). '/index');
		}
	}

	function hapus(){
		/* atur parameter data */
		$arr = $this->uri->segment(4);
		$data['arr'] = $arr;

		/* membuat value */
		$this->produk->setID($data['arr']);

		/* fungsi hapus data dari database */
		$this->produk->delete_data();

		/* flashdata info error/sukses */
		$this->session->set_flashdata('pesan', config_item('deleted'));
		redirect($this->uri->segment(1). '/index');
	}
}
<?php
foreach ($data as $key => $val) {
	?>
<h3 class="page-title">Edit Produk</h3>
<?= form_open($this->uri->segment(1).'/prosess_edit/id/'. $arr); ?>
<div class="row">
	<div class="col-md-6">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Data Produk</h3>
			</div>
			<div class="panel-body">
				<input class="form-control" placeholder="Nama" type="text" name="txt_nama" value="<?= set_value('txt_nama', $val['nama_produk']) ?>">
				<?= form_error('txt_nama'); ?>
				<br>
				<input class="form-control" placeholder="Harga" type="text" name="txt_harga" value="<?= set_value('txt_harga', $val['harga']) ?>">
				<?= form_error('txt_harga'); ?>
				<br>
				<input class="form-control" placeholder="Stok" type="text" name="txt_stok" value="<?= set_value('txt_stok', $val['stok']) ?>" onkeypress="return blockNonNumbers(this, event, true, false);">
				<?= form_error('txt_stok'); ?>
				<br>
				<div style="text-align: right">
					<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Submit </button>
				</div>
			</div>
		</div>
	</div>
</div>
<?= form_close(); ?>
	<?php
}
?>
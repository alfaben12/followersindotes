<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Produk extends CI_Model{

	private $id;
	private $nama_produk;
	private $harga;
	private $Stok;

	function __construct(){parent :: __construct();}
	function getID(){return $this->id;}
	function setID($id){$this->id = $id;}
	function getNamaProduk(){return $this->nama_produk;}
	function setNamaProduk($nama_produk){$this->nama_produk = $nama_produk;}
	function getHarga(){return $this->harga;}
	function setHarga($harga){$this->harga = $harga;}
	function getStok(){return $this->stok;}
	function setStok($stok){$this->stok = $stok;}

	function insert_data(){
		$data = array(
			'nama_produk' => $this->getNamaProduk(),
			'harga' => $this->getHarga(),
			'stok' => $this->getStok(),
		);
		$this->db->insert('produk', $data);
	}

	function update_data(){
		$data = array(
			'nama_produk' => $this->getNamaProduk(),
			'harga' => $this->getHarga(),
			'stok' => $this->getStok(),
		);
		$this->db->where('id', $this->getID());
		$this->db->update('produk', $data);
	}

	function update_stok(){
		$data = array(
			'stok' => $this->getStok(),
		);
		$this->db->where('id', $this->getID());
		$this->db->update('produk', $data);
	}

	function get_all_data(){
		$this->db->select('*');
		$this->db->from('produk');
		$query = $this->db->get();
		return $query->result_array();
	}

	function ambil_data(){
		$this->db->select('*');
		$this->db->from('produk');
		$this->db->where('id', $this->getID());
		$query = $this->db->get();
		return $query->result_array();
	}

	function delete_data(){
		$this->db->where('id', $this->getID());
		$this->db->delete('produk');
	}

}
/*
Navicat MySQL Data Transfer

Source Server         : Zrav Development
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : followersindo

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2018-08-11 22:45:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for deposit
-- ----------------------------
DROP TABLE IF EXISTS `deposit`;
CREATE TABLE `deposit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(255) DEFAULT NULL,
  `nama_pengirim` varchar(255) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of deposit
-- ----------------------------
INSERT INTO `deposit` VALUES ('18', '6', 'Riska', '50000');
INSERT INTO `deposit` VALUES ('19', '6', 'Hafizkha', '15000');
INSERT INTO `deposit` VALUES ('20', '1', 'Fatin', '10000');

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_produk` varchar(255) DEFAULT NULL,
  `harga` varchar(255) DEFAULT NULL,
  `stok` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk
-- ----------------------------
INSERT INTO `produk` VALUES ('5', 'Ayam', '20000', '5');
INSERT INTO `produk` VALUES ('6', 'Sapi', '10000', '90');
INSERT INTO `produk` VALUES ('7', 'Kuda', '15000', '75');
INSERT INTO `produk` VALUES ('8', 'Kambing', '8000', '35');

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `produkid` int(11) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
INSERT INTO `transaksi` VALUES ('11', '6', '5', '20');
INSERT INTO `transaksi` VALUES ('12', '1', '8', '5');
INSERT INTO `transaksi` VALUES ('13', '1', '6', '10');
INSERT INTO `transaksi` VALUES ('14', '1', '7', '75');
INSERT INTO `transaksi` VALUES ('15', '6', '5', '25');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `saldo` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Thariq Alfa', 'alfaben12', '123456', '20000');
INSERT INTO `user` VALUES ('6', 'Ryu Dzaky', 'ryu7', '12345', '115000');
SET FOREIGN_KEY_CHECKS=1;
